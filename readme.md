# Giant Peach Popup Notice - Wordpress Plugin

It will display a popup with editable text when landing on any page of the site. This is dismissed if a link in the popup is clicked or if it is closed by saving a cookie.

It adds an ACF Options page to the admin panel where you can configure: the notice text, the close button text/html, and any pages to exclude the popup from appearing on.