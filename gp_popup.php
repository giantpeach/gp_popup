<?php
/**
 * Plugin Name: Giant Peach Popup Notice
 * Description: Adds functionality for a popup notice that appears when landing on a page
 * Version: 1.0.1
 * Author: Giant Peach
 * Author URI: https://giantpeach.agency
 * Text Domain: gp_popup
 *
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

class GP_Popup_Notice
{
    public function __construct() {
        add_action('wp_footer', [$this, 'popup_notice_html'], 10, 1);
        add_action('wp_enqueue_scripts', [$this, 'enqueue_assets'], 10, 1);
        add_action('acf/init', [$this, 'acf_options_page_init'], 10, 1);
        add_action('acf/init', [$this, 'acf_add_fields'], 10, 1);
    }

    public function enqueue_assets()
    {
        wp_enqueue_style( 'popup-notice-css', plugins_url( '/assets/dist/css/main.css', __FILE__ ), array(), '1.0.1' );
        wp_enqueue_script('popup-notice-js', plugins_url( '/assets/dist/js/main.js', __FILE__ ), array(), '1.0.1', true );
    }

    public function acf_options_page_init()
    {
        acf_add_options_page(array(
            'page_title'    => 'Popup Notice Settings',
            'menu_title'    => 'Popup Notice',
            'menu_slug'     => 'popup-settings',
            'capability'    => 'edit_posts',
            'redirect'      => false,
            'position'      => 1000,
            'icon_url'      => 'dashicons-pressthis'
        ));
    }

    public function acf_add_fields()
    {
            acf_add_local_field_group(array(
                'key' => 'gp_popup_group_5e6fa6fb08220',
                'title' => 'Popup Notice Settings',
                'fields' => array(
                    array(
                        'key' => 'gp_popup_field_5e6fa6fb628a3',
                        'label' => 'Popup Notice Text',
                        'name' => 'popup_notice_text',
                        'type' => 'wysiwyg',
                        'instructions' => 'The content that appears in the popup notice. The notice will not appear again after Close is clicked, or after any links within the popup are clicked.',
                        'required' => 0,
                        'conditional_logic' => 0,
                        'wrapper' => array(
                            'width' => '',
                            'class' => '',
                            'id' => '',
                        ),
                        'default_value' => '',
                        'tabs' => 'all',
                        'toolbar' => 'full',
                        'media_upload' => 0,
                        'delay' => 0,
                    ),
                    array(
                        'key' => 'gp_popup_field_5e70a7a647f83',
                        'label' => 'Popup Notice Close Text',
                        'name' => 'popup_notice_close_html',
                        'type' => 'text',
                        'instructions' => 'The word or html to use within the popup notice close button. Leave empty for a cross icon.',
                        'required' => 0,
                        'conditional_logic' => 0,
                        'wrapper' => array(
                            'width' => '',
                            'class' => '',
                            'id' => '',
                        ),
                        'default_value' => '',
                        'placeholder' => '',
                        'prepend' => '',
                        'append' => '',
                        'maxlength' => '',
                    ),
                    array(
                        'key' => 'gp_popup_field_5e6fac773305e',
                        'label' => 'Popup Notice Page Exclusion',
                        'name' => 'popup_notice_page_exclude',
                        'type' => 'post_object',
                        'instructions' => 'Choose any pages the popup should not appear on.',
                        'required' => 0,
                        'conditional_logic' => 0,
                        'wrapper' => array(
                            'width' => '',
                            'class' => '',
                            'id' => '',
                        ),
                        'taxonomy' => '',
                        'allow_null' => 1,
                        'multiple' => 1,
                        'return_format' => 'id',
                        'ui' => 1,
                    ),
                ),
                'location' => array(
                    array(
                        array(
                            'param' => 'options_page',
                            'operator' => '==',
                            'value' => 'popup-settings',
                        ),
                    ),
                ),
                'menu_order' => 100,
                'position' => 'normal',
                'style' => 'default',
                'label_placement' => 'top',
                'instruction_placement' => 'label',
                'hide_on_screen' => '',
                'active' => true,
                'description' => '',
            ));

    }

    /**
     * Prints cookie notice html in page footer
     */
    public function popup_notice_html() {
        $text = apply_filters('gp_popup_notice_text', get_field('popup_notice_text','options'));

        if (empty($text)) {
            return;
        }

        $id = get_the_ID();
        $close_button_html = apply_filters('gp_popup_close_button_html', get_field('popup_notice_close_html','options'));
        $close_button_class = apply_filters('gp_popup_close_button_class', '');
        $exclude = get_field('popup_notice_page_exclude','options');
        $show = true;

        if (empty($close_button_html)) {
            $close_button_class .= " icon";
        }

        if (!$exclude) {
            $exclude = array();
        }
        if (is_integer($exclude)) {
            $exclude = array($exclude);
        }

        // if current page is in list of exclusions, don't show popup
        if (in_array($id, $exclude)) {
            $show = false;
        }

        // if news page is in list of exclusions, and we are on blog home, category or tag, don't show popup
        if ($show && (is_home() || is_category() || is_tag()) && in_array(get_option( 'page_for_posts' ), $exclude)) {
            $show = false;
        }

        if ($show) {
            echo apply_filters('gp_popup_notice_html',
                sprintf('<div class="popup-notice"><div class="popup-notice__inner"><div class="popup-notice__close %s">%s</div><div class="popup-notice__content">%s</div></div></div>', $close_button_class, $close_button_html, $text ),
                $text
            );
        }

    }

}
new GP_Popup_Notice();
