const webpack = require('webpack');
const path = require('path');

const TerserJSPlugin = require('terser-webpack-plugin'); // replaces UglifyJsPlugin
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const OptimizeCSSAssetsPlugin = require('optimize-css-assets-webpack-plugin');
// const HtmlWebpackPlugin = require("html-webpack-plugin");
// const CopyWebpackPlugin = require('copy-webpack-plugin');
// const devMode = process.env.NODE_ENV !== 'production';

// const fs = require("fs");

module.exports = {
  cache: true, // set to false on windows to make --watch work!
  mode: 'development',
  devtool: 'source-map',
  entry: {
    main: ['./js/main.js', './scss/main.scss'],
  },
  output: {
    filename: 'js/[name].js',
    chunkFilename: 'js/[name]_[chunkhash].js',
    path: path.resolve(__dirname, '../dist/'),
    publicPath: '/app/plugins/gp_popup/assets/dist/',
  },
  watchOptions: {
    poll: true, // needed for NTFS file systems and VMs
    //ignored: /node_modules/
  },
  optimization: {
    minimizer: [
      new TerserJSPlugin({
        cache: true,
        parallel: true,
        sourceMap: true,
      }),
      new OptimizeCSSAssetsPlugin({
        //assetNameRegExp: /\.optimize\.css$/g,
        //cssProcessor: require('cssnano'),
        cssProcessorOptions: {
          safe: true,
          zindex: false,
          discardComments: {
            removeAll: true,
          },
        },
        canPrint: true,
      }),
    ],
  },
  plugins: [
    // Monotype (fonts) licensing requirement
    // new webpack.BannerPlugin({
    //     banner: "This CSS resource incorporates links to font software which is the valuable copyrighted property of Monotype Imaging and/or its suppliers. You may not attempt to copy, install, redistribute, convert, modify or reverse engineer this font software. Please contact Monotype Imaging with any questions regarding Web Fonts:  http://www.fonts.com",
    //     entryOnly: true
    // }),
    new MiniCssExtractPlugin({
      // Options similar to the same options in webpackOptions.output
      // both options are optional
      filename: 'css/[name].css',
      chunkFilename: 'css/[id].css',
      //path: path.resolve(__dirname, '../dist/'),
      //publicPath: '/app/themes/seed/dist/'
    }),
    // new webpack.ProvidePlugin({
    //   $: 'jquery',
    //   jQuery: 'jquery',
    //   'window.jQuery': 'jquery',
    //   Popper: ['popper.js', 'default'],
    //   Util: 'exports-loader?Util!bootstrap/js/dist/util',
    //   //Dropdown: "exports-loader?Dropdown!bootstrap/js/dist/dropdown",
    // }),
    // new CopyWebpackPlugin([
    //   {
    //     from: 'static/img',
    //     to: 'images',
    //   },
    // ])
  ],
  module: {
    rules: [
      {
        // https://gist.github.com/qodesmith/6c5c9e64fccd660673148d93b1e263e2
        test: /\.(js|jsx)$/,
        exclude: /(node_modules|bower_components)/, // This may not be needed since we supplied `include`.
        // include: path.resolve(__dirname, 'src'),
        use: { loader: 'babel-loader' },
      },
      {
        test: /\.(jpe?g|gif|png|svg)$/,
        use: {
          loader: 'file-loader?name=images/[hash].[ext]',
        },
      },
      {
        // test: /\.js$/,
        // exclude: /(node_modules|bower_components)/,
        // use: {
        //   loader: 'babel-loader',
        //   options: {
        //     presets: ['@babel/preset-env']
        //   }
        // },
        test: /\.(css|scss)$/,
        use: [
          MiniCssExtractPlugin.loader,
          // devMode ? 'style-loader' : MiniCssExtractPlugin.loader,
          {
            loader: 'css-loader', // includes css-nano
            options: {
              importLoaders: 2,
              sourceMap: true,
              alias: {
                '../fonts/bootstrap': 'bootstrap-sass/assets/fonts/bootstrap',
              },
            },
          },
          {
            loader: 'postcss-loader', // can be used with css-nano
            options: {
              plugins: () => [require('precss'), require('autoprefixer')],
              sourceMap: true,
            },
          },
          {
            loader: 'sass-loader',
            options: {
              sourceMap: true,
              includePaths: [
                path.resolve(__dirname, '../src/scss'),
                //path.resolve(__dirname, '../src/node_modules/bootstrap/scss'),
                //path.resolve("./node_modules/bootstrap-sass/assets/stylesheets")
              ],
            },
          },
        ],
      },

      // {
      //   test: /\.(eot|ttf|woff|woff2)$/,
      //   use: {
      //       loader: 'url-loader?name=fonts/[name].[ext]'
      //   }
      // },
    ],
  },
};
