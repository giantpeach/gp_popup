import Cookies from "js-cookie";
import { disableBodyScroll, enableBodyScroll } from 'body-scroll-lock';

function PopupNotice() {

    var $this = this;
    this.is_visible = false;
    this.popup_notice = document.querySelector('.popup-notice');

    if (!this.popup_notice) {
        return;
    }

    this.popup_notice_content = this.popup_notice.querySelector('.popup-notice__content');
    this.popup_notice_links = this.popup_notice.querySelectorAll('a');
    this.close_button = this.popup_notice.querySelector('.popup-notice__close');
    const isBot = !("onscroll" in window) || /glebot/.test(navigator.userAgent);

    // cookie notice affects crawling
    if (isBot) {
        return;
    }

    if (this.popup_notice !== null && !this.getCookie()) {
        // if element exists and cookie not set, open it
        window.addEventListener('load', function() {
            setTimeout(function(){ $this.open(); }, 1000);
        });
    }
    else {
        return;
    }

    for (let i = 0; i < this.popup_notice_links.length; i++) {
        var link = this.popup_notice_links[i];
        link.addEventListener('click', function() {
            $this.setCookie('accepted');
        });
    }

    if (this.close_button !== null) {
        this.close_button.addEventListener('click', function() {
            $this.setCookie('accepted');
            $this.close();
        });
    }
}

PopupNotice.prototype.setCookie = function(value) {
    Cookies.set('gp_popup_notice', value, { expires: 30, path: '/' })
}

PopupNotice.prototype.getCookie = function(value) {
    return Cookies.get('gp_popup_notice');
}

PopupNotice.prototype.open = function() {
    disableBodyScroll(this.popup_notice_content);
    this.popup_notice.classList.add('visible');
    this.is_visible = true;
}

PopupNotice.prototype.close = function() {
    enableBodyScroll(this.popup_notice_content);
    this.popup_notice.classList.remove('visible');
    this.is_visible = false;
}

export default PopupNotice;

